﻿using System;

namespace ShiftArrayElements
{
    public static class Shifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using <see cref="iterations"/> array for getting directions and iterations (see README.md for detailed instructions).
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="iterations">An array with iterations.</param>
        /// <returns>An array with shifted elements.</returns>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">iterations array is null.</exception>
        public static int[] Shift(int[]? source, int[]? iterations)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (iterations == null)
            {
                throw new ArgumentNullException(nameof(iterations));
            }

            for (int i = 0; i < source.Length; i++)
            {
                for (int j = 0; j < iterations.Length; j++)
                {
                    if (j % 2 == 0 || j == 0)
                    {
                        for (int l = 0; l < iterations[j]; l++)
                        {
                            int temp = source[0];
                            for (int k = 0; k < source.Length - 1; k++)
                            {
                                source[k] = source[k + 1];
                            }

                            source[source.Length - 1] = temp;
                        }
                    }
                    else
                    {
                        for (int l = 0; l < iterations[j]; l++)
                        {
                            var temp = source[source.Length - 1];
                            Array.Copy(source, 0, source, 1, source.Length - 1);
                            source[0] = temp;
                        }
                    }
                }
            }

            return source;
       }
    }
}
